package com.design.elegant.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 业务异常枚举
 *
 * @author xudehui1
 * @date 2023/12/2 20:53
 */
@Getter
@AllArgsConstructor
public enum BizCode {

    ERROR(-1, "失败"),
    SUCCESS(200, "成功"),
    USER_NOT_FOUND(1001, "用户未找到");

    private final Integer value;
    private final String message;

    public static String getDescByValue(Integer value) {
        if (null == value) {
            return "";
        }
        for (BizCode item : BizCode.values()) {
            if (value.equals(item.getValue())) {
                return item.getMessage();
            }
        }
        return "";
    }

    public static BizCode getEnumByValue(Integer value) {
        if (null == value) {
            return null;
        }
        for (BizCode item : BizCode.values()) {
            if (value.equals(item.getValue())) {
                return item;
            }
        }
        return null;
    }

    public static Integer getValueByDesc(String desc) {
        for (BizCode item : BizCode.values()) {
            if (item.getMessage().equals(desc)) {
                return item.getValue();
            }
        }
        return null;
    }

    public static List<Integer> getValues() {
        BizCode[] values = BizCode.values();
        return Arrays.stream(values).map(BizCode::getValue).collect(Collectors.toList());
    }
}
