package com.design.elegant.exception;

import org.springframework.util.StringUtils;

/**
 * @author: xudehui1
 * @date: 2023-12-02 20:51
 */
public class BizException extends RuntimeException {
    private BizCode bizCode;


    public BizCode getBizCode() {
        return bizCode;
    }

    public void setBizCode(BizCode bizCode) {
        this.bizCode = bizCode;
    }

    public BizException() {
        super();
        this.bizCode = BizCode.ERROR;
    }

    /**
     * BizException
     *
     * @param code    BizCode
     * @param message 错误信息
     */
    public BizException(BizCode code, String message) {
        super(message);
        this.bizCode = code;
    }

    /**
     * BizException
     *
     * @param code BizCode
     */
    public BizException(BizCode code) {
        super(code.getMessage());
        this.bizCode = code;
    }

    /**
     * BizException
     *
     * @param code    BizCode
     * @param message 错误信息
     * @param cause   Throwable
     */
    public BizException(BizCode code, String message, Throwable cause) {
        super(message, cause);
        this.bizCode = code;
    }

    @Override
    public String getMessage() {
        return StringUtils.isEmpty(super.getMessage()) ? this.bizCode.getMessage() : super.getMessage();
    }

    /**
     * 重载自定义异常，提高性能
     *
     * @return this
     */
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
