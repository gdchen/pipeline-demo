package com.design.elegant.pipeline.creator;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.design.elegant.pipeline.EventFilter;
import com.design.elegant.pipeline.annotation.EventFilterAnnotation;
import com.design.elegant.pipeline.common.BaseEnum;
import com.design.elegant.pipeline.config.FilterConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 负责读取和解析过滤器配置（本地或远程
 *
 * @author coderxdh
 * @date 2023/12/10 22:16
 */
@Component
@Slf4j
public class FilterConfigReader {

    private final ApplicationContext applicationContext;
    private final FilterConfiguration filterConfiguration;

    @Autowired
    public FilterConfigReader(ApplicationContext applicationContext, FilterConfiguration filterConfiguration) {
        this.applicationContext = applicationContext;
        this.filterConfiguration = filterConfiguration;
    }

    public List<FilterConfiguration.FilterDefinition> getFilterDefinitionsFromLocal(BaseEnum baseEnum) {
        List<FilterConfiguration.FilterDefinition> definitions = filterConfiguration.getBusinessFilters().get(baseEnum.getCode());
        if (definitions == null) {
            throw new IllegalStateException("No filter definitions found for business code: " + baseEnum.getCode());
        }
        return definitions;
    }

    public List<FilterConfiguration.FilterDefinition> getFilterDefinitionsFromRemote(String jsonStrConfig, BaseEnum baseEnum) {
        FilterConfiguration filterConfiguration = null;
        try {
            filterConfiguration = JSON.parseObject(jsonStrConfig, new TypeReference<FilterConfiguration>() {
            });
        } catch (Exception e) {
            log.error("Serialization exception occurred for remote config: {}", jsonStrConfig, e);
            throw new RuntimeException("Serialization exception occurred for remote config");
        }

        Map<String, List<FilterConfiguration.FilterDefinition>> businessFilters = filterConfiguration.getBusinessFilters();
        List<FilterConfiguration.FilterDefinition> definitions = businessFilters.get(baseEnum.getCode());
        if (definitions == null) {
            throw new IllegalStateException("No filter definitions found for business code: " + baseEnum.getCode());
        }
        return definitions;
    }

    public List<Class<?>> getFilterDefinitionsFromAnno(BaseEnum baseEnum) {
        List<Class<?>> filterClasses = Arrays.asList(applicationContext.getBeanNamesForType(EventFilter.class))
                .stream()
                .map(beanName -> applicationContext.getType(beanName))
                .collect(Collectors.toList());

        List<Class<?>> definitions = filterClasses.stream()
                .filter(filterClass -> {
                    EventFilterAnnotation annotation = filterClass.getAnnotation(EventFilterAnnotation.class);
                    return annotation != null && baseEnum.getCode().equals(annotation.bizCode());
                }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(definitions)) {
            throw new IllegalStateException("No filter definitions found for business code: " + baseEnum.getCode());
        }

        return definitions;
    }
}
