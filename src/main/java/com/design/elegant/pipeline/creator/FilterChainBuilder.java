package com.design.elegant.pipeline.creator;

import com.design.elegant.pipeline.EventFilter;
import com.design.elegant.pipeline.FilterChainPipeline;
import com.design.elegant.pipeline.config.FilterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 负责构建和组装过滤器链
 *
 * @author coderxdh
 * @date 2023/12/10 22:12
 */
@Component
public class FilterChainBuilder {

    private final FilterInstantiator filterInstantiator;

    @Autowired
    public FilterChainBuilder(FilterInstantiator filterInstantiator) {
        this.filterInstantiator = filterInstantiator;
    }

    public FilterChainPipeline<EventFilter> buildPipelineFromDefinition(List<FilterConfiguration.FilterDefinition> definitions) {
        FilterChainPipeline<EventFilter> pipeline = new FilterChainPipeline<>();
        definitions.stream()
                .sorted(Comparator.comparingInt(FilterConfiguration.FilterDefinition::getPriority))
                .forEachOrdered(definition -> {
                    EventFilter filter = filterInstantiator.createFilter(definition);
                    pipeline.addFirst(filter);
                });
        return pipeline;
    }

    public FilterChainPipeline<EventFilter> buildPipelineFromAnnoClass(List<Class<?>> definitions) {
        FilterChainPipeline<EventFilter> pipeline = new FilterChainPipeline<>();
        List<EventFilter> eventFilterList = definitions.stream()
                .map(filterInstantiator::createFilter)
                .sorted(Comparator.comparingInt(EventFilter::getPriority))
                .collect(Collectors.toList());
        eventFilterList.forEach(filter -> pipeline.addFirst(filter));
        return pipeline;
    }
}
