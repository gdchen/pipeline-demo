package com.design.elegant.pipeline.factory;

import com.design.elegant.pipeline.EventFilter;
import com.design.elegant.pipeline.FilterChainPipeline;
import com.design.elegant.pipeline.cache.FilterChainCache;
import com.design.elegant.pipeline.common.BaseEnum;
import com.design.elegant.pipeline.config.FilterConfiguration;
import com.design.elegant.pipeline.creator.FilterChainBuilder;
import com.design.elegant.pipeline.creator.FilterConfigReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 事件过滤器链工厂，负责根据不同场景创建过滤器链
 *
 * @author: xudehui1
 * @date: 2023-12-02 15:51
 */

@Component
@Slf4j
public class FilterFactory {

    private final FilterChainBuilder filterChainBuilder;
    private final FilterConfigReader filterConfigReader;

    @Autowired
    public FilterFactory(FilterChainBuilder filterChainBuilder, FilterConfigReader filterConfigReader) {
        this.filterChainBuilder = filterChainBuilder;
        this.filterConfigReader = filterConfigReader;
    }

    /**
     * 基于读取配置文件+业务编码动态组装过滤器链
     *
     * @param baseEnum
     * @return
     */
    public FilterChainPipeline createPipelineFromLocalConfig(BaseEnum baseEnum) {
        String bizCode = baseEnum.getCode();
        if (FilterChainCache.containsKey(bizCode)) {
            return FilterChainCache.get(bizCode);
        }
        List<FilterConfiguration.FilterDefinition> definitions = filterConfigReader.getFilterDefinitionsFromLocal(baseEnum);
        if (definitions == null) {
            throw new IllegalStateException("No filter definitions found for business code: " + baseEnum.getCode());
        }
        FilterChainPipeline<EventFilter> pipeline = filterChainBuilder.buildPipelineFromDefinition(definitions);
        FilterChainCache.put(bizCode, pipeline);
        return pipeline;
    }

    /**
     * 基于传入的过滤器配置对象(JSON 格式)+业务编码动态组装过滤器链
     *
     * @param jsonStrConfig
     * @param baseEnum
     * @return
     */
    public FilterChainPipeline createPipelineFromRemoteConfig(String jsonStrConfig, BaseEnum baseEnum) {
        List<FilterConfiguration.FilterDefinition> definitions = filterConfigReader.getFilterDefinitionsFromRemote(jsonStrConfig, baseEnum);
        return filterChainBuilder.buildPipelineFromDefinition(definitions);
    }


    public FilterChainPipeline createFilterChainPipelineWithAnno(BaseEnum baseEnum) {
        String bizCode = baseEnum.getCode();
        if (FilterChainCache.containsKey(bizCode)) {
            return FilterChainCache.get(bizCode);
        }
        List<Class<?>> definitions = filterConfigReader.getFilterDefinitionsFromAnno(baseEnum);
        FilterChainPipeline<EventFilter> pipeline = filterChainBuilder.buildPipelineFromAnnoClass(definitions);
        FilterChainCache.put(bizCode, pipeline);
        return pipeline;
    }
}

