package com.design.elegant.service;

import com.design.elegant.charge.Address;
import com.design.elegant.charge.Car;

public interface IFacadeService {

  Car getCarInfoByCarNO(String carNo);

  Address getAddressByAddressId(Long addressId);
}
