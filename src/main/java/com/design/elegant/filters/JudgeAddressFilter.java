package com.design.elegant.filters;

import com.design.elegant.charge.Address;
import com.design.elegant.charge.ChargeContext;
import com.design.elegant.charge.ChargeModel;
import com.design.elegant.charge.ChargeRequest;
import com.design.elegant.pipeline.AbstractEventFilter;
import com.design.elegant.pipeline.annotation.EventFilterAnnotation;
import com.design.elegant.service.IFacadeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

@Slf4j
@Component
@EventFilterAnnotation(bizCode = "YW3", priority = 1)
public class JudgeAddressFilter extends AbstractEventFilter<ChargeContext> {

  @Override
  protected void handle(ChargeContext context) {
    log.info("{}-current-thread-name: {}", this.getClass().getSimpleName(), Thread.currentThread().getName());
    Address address = context.getAddress();

    log.info("地址信息：{}", address.getId());
  }
}
