package com.design.elegant.filters;

import com.design.elegant.charge.ChargeContext;
import com.design.elegant.exception.BizCode;
import com.design.elegant.exception.BizException;
import com.design.elegant.pipeline.AbstractEventFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserPayFilter extends AbstractEventFilter<ChargeContext> {

    @Override
    protected void handle(ChargeContext context) {
        log.info("{}-current-thread-name: {}", this.getClass().getSimpleName(), Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("支付判断逻辑");
        if (context.getChargeRequest().getUserId() == null) {
            throw new BizException(BizCode.USER_NOT_FOUND);
        }
    }
}
