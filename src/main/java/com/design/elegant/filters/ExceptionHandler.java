package com.design.elegant.filters;

import com.design.elegant.exception.BizException;
import com.design.elegant.pipeline.context.EventContext;
import com.design.elegant.pipeline.exception.ErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: xudehui1
 * @date: 2023-12-02 20:58
 */
@Slf4j
@Component
public class ExceptionHandler implements ErrorHandler {
    @Override
    public void handleError(Exception e, EventContext context) throws Exception {
        if (e instanceof BizException) {
            // 有些业务异常不能直接抛给调用方
            // 比如：发生了某一个业务异常，前端需要根据这个异常场景进行处理，那么就不能直接给前端返回 success = false，需要 success =  true，然后根据 context 中某些值进行业务处理
            log.error("使用 ExceptionHandler 进行异常处理-发生了业务异常-可以针对异常码进行业务逻辑-异常码：{}", ((BizException) e).getBizCode());
        } else {
            log.error("异常：", e);
            throw e;
        }
    }
}
