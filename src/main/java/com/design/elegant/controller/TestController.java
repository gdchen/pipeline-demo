package com.design.elegant.controller;

import com.alibaba.fastjson2.JSON;
import com.design.elegant.charge.ChargeRequest;
import com.design.elegant.service.IChargeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: xudehui1
 * @date: 2023-12-05 20:51
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @Resource
    private IChargeService chargeService;

    @PostMapping("/testCharge")
    public void testCharge(@RequestBody ChargeRequest request) {
        log.info("请求入参：{}", JSON.toJSON(request));
        chargeService.handle(request);
    }

    @PostMapping("/testChargeWithAnno")
    public void testChargeWithAnno(@RequestBody ChargeRequest request) {
        log.info("请求入参：{}", JSON.toJSON(request));
        chargeService.handleWithAnno(request);
    }
}
