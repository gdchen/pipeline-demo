package com.design.elegant.matcher;

import lombok.Data;

@Data
public class UserContext {

  private String name;

  private Integer age;

}
