package com.design.elegant.charge;

import com.design.elegant.pipeline.context.AbstractEventContext;
import lombok.Getter;
import lombok.Setter;


public class ChargeContext extends AbstractEventContext {

  @Setter
  @Getter
  private ChargeRequest chargeRequest;

  @Setter
  @Getter
  private Car car;

  @Getter
  @Setter
  private User user;

  @Getter
  @Setter
  private Address address;

  @Setter
  @Getter
  private ChargeModel chargeModel;

  public ChargeContext(BizEnum bizEnum) {
    super(bizEnum);
  }
}
