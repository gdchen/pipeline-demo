package com.design.elegant.charge;

import lombok.Data;

@Data
public class ChargeRequest2 {

  private String carNo;

  private String extraInfo;

}
