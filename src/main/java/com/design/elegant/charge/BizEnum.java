package com.design.elegant.charge;

import com.design.elegant.pipeline.common.BaseEnum;

import java.util.Optional;

public enum BizEnum implements BaseEnum<BizEnum> {

  TRAFFIC_EVENT("YW1", "业务1"),
  METRIC_EVENT("YW2","业务2"),
  SIGNAL_EVENT("YW3","业务3")
  ;

  BizEnum(String code, String name) {
    this.code = code;
    this.name = name;
  }

  private String code;
  private String name;

  @Override
  public String getCode() {
    return this.code;
  }

  @Override
  public String getName() {
    return this.name;
  }

  public static Optional<BizEnum> of(String code) {
    return Optional.ofNullable(BaseEnum.parseByCode(BizEnum.class, code));
  }

}
